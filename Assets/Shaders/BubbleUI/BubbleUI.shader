// Upgrade NOTE: upgraded instancing buffer 'Bubbles' to new syntax.

// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Bubbles"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_BubblesTexture("Bubbles Texture", 2D) = "white" {}
		_BubblesDirection("BubblesDirection", Vector) = (1,0,0,0)
		_Speed("Speed", Float) = 1
		_BubbleSize("Bubble Size", Float) = 1
		_Fizz("Fizz", Float) = 0
		_Scale("Scale", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
	}

	SubShader
	{
		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"
			#pragma multi_compile_instancing

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform float _Scale;
			uniform float _Fizz;
			uniform sampler2D _BubblesTexture;
			UNITY_INSTANCING_BUFFER_START(Bubbles)
				UNITY_DEFINE_INSTANCED_PROP(float4, _MainTex_ST)
#define _MainTex_ST_arr Bubbles
				UNITY_DEFINE_INSTANCED_PROP(float2, _BubblesDirection)
#define _BubblesDirection_arr Bubbles
				UNITY_DEFINE_INSTANCED_PROP(float, _BubbleSize)
#define _BubbleSize_arr Bubbles
				UNITY_DEFINE_INSTANCED_PROP(float, _Speed)
#define _Speed_arr Bubbles
			UNITY_INSTANCING_BUFFER_END(Bubbles)
			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			
			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				float4 _MainTex_ST_Instance = UNITY_ACCESS_INSTANCED_PROP(_MainTex_ST_arr, _MainTex_ST);
				float2 uv_MainTex = IN.texcoord.xy * _MainTex_ST_Instance.xy + _MainTex_ST_Instance.zw;
				float4 tex2DNode2 = tex2D( _MainTex, uv_MainTex );
				float _BubbleSize_Instance = UNITY_ACCESS_INSTANCED_PROP(_BubbleSize_arr, _BubbleSize);
				float2 temp_output_24_0 = ( float2( 4,1 ) * _BubbleSize_Instance );
				float2 uv028 = IN.texcoord.xy * ( temp_output_24_0 * _Scale ) + float2( 0,0 );
				float cos27 = cos( ( _Fizz * _Time.y ) );
				float sin27 = sin( ( _Fizz * _Time.y ) );
				float2 rotator27 = mul( uv028 - float2( 1,0.5 ) , float2x2( cos27 , -sin27 , sin27 , cos27 )) + float2( 1,0.5 );
				float simplePerlin2D25 = snoise( rotator27*3.23 );
				simplePerlin2D25 = simplePerlin2D25*0.5 + 0.5;
				float4 temp_cast_0 = (simplePerlin2D25).xxxx;
				float2 _BubblesDirection_Instance = UNITY_ACCESS_INSTANCED_PROP(_BubblesDirection_arr, _BubblesDirection);
				float _Speed_Instance = UNITY_ACCESS_INSTANCED_PROP(_Speed_arr, _Speed);
				float2 uv015 = IN.texcoord.xy * temp_output_24_0 + ( _BubblesDirection_Instance * ( _Time.y * _Speed_Instance ) );
				float4 lerpResult38 = lerp( temp_cast_0 , tex2D( _BubblesTexture, uv015 ) , simplePerlin2D25);
				float4 lerpResult14 = lerp( tex2DNode2 , lerpResult38 , lerpResult38);
				float4 lerpResult32 = lerp( tex2DNode2 , lerpResult14 , tex2DNode2);
				float4 blendOpSrc13 = tex2DNode2;
				float4 blendOpDest13 = lerpResult32;
				
				half4 color = ( saturate( 	max( blendOpSrc13, blendOpDest13 ) ));
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=17101
-1536;0;1536;803;2033.237;185.0757;2.230731;True;True
Node;AmplifyShaderEditor.CommentaryNode;31;-2716.894,-144.4416;Inherit;False;1360.827;913.4983;Bubble Movement;10;18;20;19;17;16;8;15;24;22;23;;1,1,1,1;0;0
Node;AmplifyShaderEditor.Vector2Node;22;-2433.197,60.10507;Inherit;False;Constant;_Size;Size;4;0;Create;True;0;0;False;0;4,1;1,10;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;23;-2455.197,220.1051;Inherit;False;InstancedProperty;_BubbleSize;Bubble Size;3;0;Create;True;0;0;False;0;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-2590.376,643.0984;Inherit;False;InstancedProperty;_Speed;Speed;2;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;16;-2628.561,543.1478;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-2129.197,161.105;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-2411.669,1026.021;Inherit;False;Property;_Scale;Scale;5;0;Create;True;0;0;False;0;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;43;-2098.666,1319.441;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;41;-2107.03,1106.425;Inherit;False;Property;_Fizz;Fizz;4;0;Create;True;0;0;False;0;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;45;-2127.871,892.651;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;17;-2483.651,394.499;Inherit;False;InstancedProperty;_BubblesDirection;BubblesDirection;1;0;Create;True;0;0;False;0;1,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-2374.263,595.3088;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;28;-1888.653,793.8523;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-1840.463,1184.858;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;18;-2218.513,432.5698;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RotatorNode;27;-1575.624,851.5742;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;1,0.5;False;2;FLOAT;0.05;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;15;-1990.703,399.8027;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;37;-1247.018,-332.5417;Inherit;False;665;371;Get Sprite Texture;2;2;5;;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;8;-1677.423,281.8639;Inherit;True;Property;_BubblesTexture;Bubbles Texture;0;0;Create;True;0;0;False;0;df623d30a58e6264981e22d0c2e59d3f;df623d30a58e6264981e22d0c2e59d3f;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;25;-1170.174,708.3602;Inherit;False;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;3.23;False;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateShaderPropertyNode;5;-1177.277,-198.3517;Inherit;False;0;0;_MainTex;Shader;0;5;SAMPLER2D;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;38;-933.279,327.3349;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;2;-921.667,-197.5888;Inherit;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;False;0;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;35;-673.7484,152.7992;Inherit;False;313.2;335.3;Mask by alpha on bubbles;1;14;;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;34;-326.1859,155.4209;Inherit;False;350;334;Mask by Shapec;1;32;;1,1,1,1;0;0
Node;AmplifyShaderEditor.LerpOp;14;-588.1917,253.2405;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;32;-235.5071,268.8133;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.BlendOpsNode;13;115.7183,102.7612;Inherit;False;Lighten;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;40;361.8994,700.9448;Float;False;True;2;ASEMaterialInspector;0;4;Bubbles;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;True;2;False;-1;True;True;True;True;True;0;True;-9;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;0;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;0
Node;AmplifyShaderEditor.CommentaryNode;36;-3645.784,-498.1298;Inherit;False;1636.565;132;Comment;0;;1,1,1,1;0;0
WireConnection;24;0;22;0
WireConnection;24;1;23;0
WireConnection;45;0;24;0
WireConnection;45;1;44;0
WireConnection;20;0;16;0
WireConnection;20;1;19;0
WireConnection;28;0;45;0
WireConnection;42;0;41;0
WireConnection;42;1;43;0
WireConnection;18;0;17;0
WireConnection;18;1;20;0
WireConnection;27;0;28;0
WireConnection;27;2;42;0
WireConnection;15;0;24;0
WireConnection;15;1;18;0
WireConnection;8;1;15;0
WireConnection;25;0;27;0
WireConnection;38;0;25;0
WireConnection;38;1;8;0
WireConnection;38;2;25;0
WireConnection;2;0;5;0
WireConnection;14;0;2;0
WireConnection;14;1;38;0
WireConnection;14;2;38;0
WireConnection;32;0;2;0
WireConnection;32;1;14;0
WireConnection;32;2;2;0
WireConnection;13;0;2;0
WireConnection;13;1;32;0
WireConnection;40;0;13;0
ASEEND*/
//CHKSM=5C09D8FB3C80503A9949B5F4871819033DD3E87A