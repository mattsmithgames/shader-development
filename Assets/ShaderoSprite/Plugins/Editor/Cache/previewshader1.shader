//////////////////////////////////////////////////////////////
/// Shadero Sprite: Sprite Shader Editor - by VETASOFT 2018 //
/// Shader generate with Shadero 1.9.6                      //
/// http://u3d.as/V7t #AssetStore                           //
/// http://www.shadero.com #Docs                            //
//////////////////////////////////////////////////////////////

Shader "Shadero Previews/PreviewXATXQ1"
{
Properties
{
[PerRendererData] _MainTex("Sprite Texture", 2D) = "white" {}
OffsetUV_X_1("OffsetUV_X_1", Range(-1, 1)) = 0
OffsetUV_Y_1("OffsetUV_Y_1", Range(-1, 1)) = 0
OffsetUV_ZoomX_1("OffsetUV_ZoomX_1", Range(0.1, 10)) = 2
OffsetUV_ZoomY_1("OffsetUV_ZoomY_1", Range(0.1, 10)) = 2
AnimatedOffsetUV_X_1("AnimatedOffsetUV_X_1", Range(-1, 1)) = 0.15
AnimatedOffsetUV_Y_1("AnimatedOffsetUV_Y_1", Range(-1, 1)) = 0
AnimatedOffsetUV_ZoomX_1("AnimatedOffsetUV_ZoomX_1", Range(1, 10)) = 1
AnimatedOffsetUV_ZoomY_1("AnimatedOffsetUV_ZoomY_1", Range(1, 10)) = 1
AnimatedOffsetUV_Speed_1("AnimatedOffsetUV_Speed_1", Range(-1, 1)) = 0.1
_NewTex_1("NewTex_1(RGB)", 2D) = "white" { }
AnimatedRotationUV_AnimatedRotationUV_Rotation_1("AnimatedRotationUV_AnimatedRotationUV_Rotation_1", Range(-360, 360)) = 54
AnimatedRotationUV_AnimatedRotationUV_PosX_1("AnimatedRotationUV_AnimatedRotationUV_PosX_1", Range(-1, 2)) = 0.5
AnimatedRotationUV_AnimatedRotationUV_PosY_1("AnimatedRotationUV_AnimatedRotationUV_PosY_1", Range(-1, 2)) = 0.5
AnimatedRotationUV_AnimatedRotationUV_Intensity_1("AnimatedRotationUV_AnimatedRotationUV_Intensity_1", Range(0, 4)) = 0.5
AnimatedRotationUV_AnimatedRotationUV_Speed_1("AnimatedRotationUV_AnimatedRotationUV_Speed_1", Range(-10, 10)) = 0.75
_Generate_Voronoi_Size_1("_Generate_Voronoi_Size_1", Range(0, 128)) = 8
_Generate_Voronoi_Seed_1("_Generate_Voronoi_Seed_1", Range(1, 2)) = 1
_MaskAlpha_Fade_1("_MaskAlpha_Fade_1", Range(0, 1)) = 0
_SpriteFade("SpriteFade", Range(0, 1)) = 1.0

// required for UI.Mask
[HideInInspector]_StencilComp("Stencil Comparison", Float) = 8
[HideInInspector]_Stencil("Stencil ID", Float) = 0
[HideInInspector]_StencilOp("Stencil Operation", Float) = 0
[HideInInspector]_StencilWriteMask("Stencil Write Mask", Float) = 255
[HideInInspector]_StencilReadMask("Stencil Read Mask", Float) = 255
[HideInInspector]_ColorMask("Color Mask", Float) = 15

}

SubShader
{

Tags {"Queue" = "Transparent" "IgnoreProjector" = "true" "RenderType" = "Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
ZWrite Off Blend SrcAlpha OneMinusSrcAlpha Cull Off 

// required for UI.Mask
Stencil
{
Ref [_Stencil]
Comp [_StencilComp]
Pass [_StencilOp]
ReadMask [_StencilReadMask]
WriteMask [_StencilWriteMask]
}

Pass
{

CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma fragmentoption ARB_precision_hint_fastest
#include "UnityCG.cginc"

struct appdata_t{
float4 vertex   : POSITION;
float4 color    : COLOR;
float2 texcoord : TEXCOORD0;
};

struct v2f
{
float2 texcoord  : TEXCOORD0;
float4 vertex   : SV_POSITION;
float4 color    : COLOR;
};

sampler2D _MainTex;
float _SpriteFade;
float OffsetUV_X_1;
float OffsetUV_Y_1;
float OffsetUV_ZoomX_1;
float OffsetUV_ZoomY_1;
float AnimatedOffsetUV_X_1;
float AnimatedOffsetUV_Y_1;
float AnimatedOffsetUV_ZoomX_1;
float AnimatedOffsetUV_ZoomY_1;
float AnimatedOffsetUV_Speed_1;
sampler2D _NewTex_1;
float AnimatedRotationUV_AnimatedRotationUV_Rotation_1;
float AnimatedRotationUV_AnimatedRotationUV_PosX_1;
float AnimatedRotationUV_AnimatedRotationUV_PosY_1;
float AnimatedRotationUV_AnimatedRotationUV_Intensity_1;
float AnimatedRotationUV_AnimatedRotationUV_Speed_1;
float _Generate_Voronoi_Size_1;
float _Generate_Voronoi_Seed_1;
float _MaskAlpha_Fade_1;

v2f vert(appdata_t IN)
{
v2f OUT;
OUT.vertex = UnityObjectToClipPos(IN.vertex);
OUT.texcoord = IN.texcoord;
OUT.color = IN.color;
return OUT;
}


float2 AnimatedOffsetUV(float2 uv, float offsetx, float offsety, float zoomx, float zoomy, float speed)
{
speed *=_Time*25;
uv += float2(offsetx*speed, offsety*speed);
uv = fmod(uv * float2(zoomx, zoomy), 1);
return uv;
}
float2 OffsetUV(float2 uv, float offsetx, float offsety, float zoomx, float zoomy)
{
uv += float2(offsetx, offsety);
uv = fmod(uv * float2(zoomx, zoomy), 1);
return uv;
}

float2 OffsetUVClamp(float2 uv, float offsetx, float offsety, float zoomx, float zoomy)
{
uv += float2(offsetx, offsety);
uv = fmod(clamp(uv * float2(zoomx, zoomy), 0.0001, 0.9999), 1);
return uv;
}
float3 Generate_Voronoi_hash3(float2 p, float seed)
{
float3 q = float3(dot(p, float2(127.1, 311.7)),
dot(p, float2(269.5, 183.3)),
dot(p, float2(419.2, 371.9)));
return frac(sin(q) * 43758.5453 * seed);
}
float4 Generate_Voronoi(float2 uv, float size,float seed, float black)
{
float2 p = floor(uv*size);
float2 f = frac(uv*size);
float k = 1.0 + 63.0*pow(1.0, 4.0);
float va = 0.0;
float wt = 0.0;
for (int j = -2; j <= 2; j++)
{
for (int i = -2; i <= 2; i++)
{
float2 g = float2(float(i), float(j));
float3 o = Generate_Voronoi_hash3(p + g, seed)*float3(1.0, 1.0, 1.0);
float2 r = g - f + o.xy;
float d = dot(r, r);
float ww = pow(1.0 - smoothstep(0.0, 1.414, sqrt(d)), k);
va += o.z*ww;
wt += ww;
}
}
float4 result = saturate(va / wt);
result.a = saturate(result.a + black);
return result;
}
float2 AnimatedRotationUV(float2 uv, float rot, float posx, float posy, float radius, float speed)
{
uv = uv - float2(posx, posy);
float angle = rot * 0.01744444;
angle += sin(_Time * speed * 20) * radius;
float sinX = sin(angle);
float cosX = cos(angle);
float2x2 rotationMatrix = float2x2(cosX, -sinX, sinX, cosX);
uv = mul(uv, rotationMatrix) + float2(posx, posy);
return uv;
}
float4 frag (v2f i) : COLOR
{
float2 OffsetUV_1 = OffsetUV(i.texcoord,OffsetUV_X_1,OffsetUV_Y_1,OffsetUV_ZoomX_1,OffsetUV_ZoomY_1);
float2 AnimatedOffsetUV_1 = AnimatedOffsetUV(OffsetUV_1,AnimatedOffsetUV_X_1,AnimatedOffsetUV_Y_1,AnimatedOffsetUV_ZoomX_1,AnimatedOffsetUV_ZoomY_1,AnimatedOffsetUV_Speed_1);
float4 NewTex_1 = tex2D(_NewTex_1,AnimatedOffsetUV_1);
float2 AnimatedRotationUV_1 = AnimatedRotationUV(i.texcoord,AnimatedRotationUV_AnimatedRotationUV_Rotation_1,AnimatedRotationUV_AnimatedRotationUV_PosX_1,AnimatedRotationUV_AnimatedRotationUV_PosY_1,AnimatedRotationUV_AnimatedRotationUV_Intensity_1,AnimatedRotationUV_AnimatedRotationUV_Speed_1);
float4 _Generate_Voronoi_1 = Generate_Voronoi(AnimatedRotationUV_1,_Generate_Voronoi_Size_1,_Generate_Voronoi_Seed_1,0);
float4 MaskAlpha_1=NewTex_1;
MaskAlpha_1.a = lerp(_Generate_Voronoi_1.a, 1 - _Generate_Voronoi_1.a,_MaskAlpha_Fade_1);
float4 FinalResult = MaskAlpha_1;
FinalResult.rgb *= i.color.rgb;
FinalResult.a = FinalResult.a * _SpriteFade * i.color.a;
return FinalResult;
}

ENDCG
}
}
Fallback "Sprites/Default"
}
